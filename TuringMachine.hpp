//
//  TuringMachine.hpp
//  TuringMachine
//
//  Created by Anastasiia Andreichuk on 6/14/17.
//  Copyright © 2017 Anastasiia Andreichuk. All rights reserved.
//

#ifndef TuringMachine_hpp
#define TuringMachine_hpp

#include <stdio.h>
#include "types.h"
#include <deque>
#include <vector>
#include <string>

class TuringMachine {
public:
    TuringMachine(const std::string& inputFile);
    
    void setTape(const std::string& inputTape);
    void setShowTape(bool showTape);
    void run();
    void print(const std::string& outputFile) const;
    void print() const; // std::cout
    
private:
    void readInputFile();
    Rule parseInputLine(const std::string& inputLine);
    void updateRules(const Rule& rule);
    void process();
    
    void print(std::ostream& ost) const;
    void printTape() const;
    
    int findEndTapeCellIdx() const;

    const char BLANK_SYMBOL = '0';
    const int INIT_STATES_NUMBER = 25;
    const int INIT_TAPE_SIZE = 100;
    const int MAX_ITERATIONS = 100000000;
    
    std::string inputFile_;
    
    std::deque<char> tape_;
    std::vector<std::vector<CellRule>> cellRules_; // 0 - terminating; other states: inputState + 1

    int tapeSize_;
    int statesNumber_;
    bool showTape_;
    
    bool isInputTapeRead_;
    int currentState_;
    int numIterations_;
    int currentTapeCellIdx_;
};

std::string firstMatch(std::string& str, const std::string& regex);

#endif /* TuringMachine_hpp */
