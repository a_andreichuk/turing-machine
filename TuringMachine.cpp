//
//  TuringMachine.cpp
//  TuringMachine
//
//  Created by Anastasiia Andreichuk on 6/14/17.
//  Copyright © 2017 Anastasiia Andreichuk. All rights reserved.
//

#include "TuringMachine.hpp"
#include <iostream>
#include <ctype.h>
#include <fstream>
#include <algorithm>
#include <regex>

TuringMachine::TuringMachine(const std::string& inputFile)
    : inputFile_(inputFile),
      currentTapeCellIdx_(0),
      currentState_(1),
      statesNumber_(INIT_STATES_NUMBER),
      tapeSize_(INIT_TAPE_SIZE),
      isInputTapeRead_(false),
      numIterations_(0),
      showTape_(false) {
    tape_.resize(tapeSize_);
    std::fill(tape_.begin(), tape_.end(), BLANK_SYMBOL);
    cellRules_.resize(statesNumber_);
}

void TuringMachine::readInputFile() {
    std::ifstream ifs(inputFile_);
    std::string inputLine;
    
    while (getline (ifs, inputLine)) {
        if (inputLine.empty())
            continue;
        
        if (inputLine[0] != 'q') {
            setTape(inputLine);
        } else {
            Rule rule = parseInputLine(inputLine);
            updateRules(rule);
        }
    }
    
    ifs.close();
}

void TuringMachine::updateRules(const Rule& rule) {
    if (rule.startState >= statesNumber_ || rule.cellRule.endState >= statesNumber_) {
        int newStatesNumber = std::max(rule.startState, rule.cellRule.endState) + 1;
        cellRules_.resize(newStatesNumber);
        statesNumber_ = newStatesNumber;
    }
    
    cellRules_[rule.startState].push_back(rule.cellRule);
}

void TuringMachine::setTape(const std::string& inputTape) {
    if (isInputTapeRead_)
        throw std::logic_error("Input tape is already read.");
    
    for (int tapeCellIdx = 0; tapeCellIdx < inputTape.size(); ++tapeCellIdx)
        tape_[tapeCellIdx] = inputTape[tapeCellIdx];
    
    isInputTapeRead_ = true;
}

void TuringMachine::run() {
    readInputFile();
    
    if (isInputTapeRead_)
        process();
    else
        throw std::logic_error("Input tape is already read.");
}

void TuringMachine::process() {
    if (showTape_)
        printTape();
    
    while (currentState_ != 0 && numIterations_ < MAX_ITERATIONS) {
        CellRule cellRule;
        bool isRuleFound = false;
        
        for (const auto& currentCellRule : cellRules_[currentState_])
            if (currentCellRule.isActive && currentCellRule.startSymbol == tape_[currentTapeCellIdx_]) {
                cellRule = currentCellRule;
                isRuleFound = true;
                break;
            }
        
        if (!isRuleFound)
            throw std::logic_error("Rule is not found.");
        
        tape_[currentTapeCellIdx_] = cellRule.endSymbol;
        
        if (cellRule.direction == RIGHT) {
            if (currentTapeCellIdx_ == tapeSize_ - 1) {
                tape_.push_back(BLANK_SYMBOL);
                ++tapeSize_;
            }
            
            ++currentTapeCellIdx_;
        } else if (cellRule.direction == LEFT) {
            if (currentTapeCellIdx_ == 0) {
                tape_.push_front(BLANK_SYMBOL);
                ++tapeSize_;
            } else {
                --currentTapeCellIdx_;
            }
        }
        
        currentState_ = cellRule.endState;
        ++numIterations_;
        
        if (showTape_)
            printTape();
    }
}

void TuringMachine::setShowTape(bool showTape) {
    showTape_ = showTape;
}

void TuringMachine::print(const std::string& outputFile) const {
    std::ofstream ofs(outputFile);
    print(ofs);
    ofs.close();
}

void TuringMachine::print() const {
    print(std::cout);
}

int TuringMachine::findEndTapeCellIdx() const {
    int endTapeCellIdx = 0;
    
    for (int tapeCellIdx = tapeSize_ - 1; tapeCellIdx > 0; --tapeCellIdx)
        if (tape_[tapeCellIdx] != BLANK_SYMBOL) {
            endTapeCellIdx = tapeCellIdx;
            break;
        }

    return endTapeCellIdx;
}

void TuringMachine::printTape() const {
    int endTapeCellIdx = findEndTapeCellIdx();
    
    for (int tapeCellIdx = 0; tapeCellIdx < endTapeCellIdx + 1; ++tapeCellIdx)
        std::cout << tape_[tapeCellIdx];
    
    std::cout << std::endl;
    
    for (int tapeCellIdx = 0; tapeCellIdx < endTapeCellIdx + 2; ++tapeCellIdx) {
        if (tapeCellIdx != currentTapeCellIdx_)
            std::cout << ' ';
        else
            std::cout << '^';
    }
    
    std::cout << std::endl << 'q';
    
    if (currentState_ == 0)
        std::cout << '*';
    else
        std::cout << currentState_ - 1;
    
    std::cout << std::endl << std::endl << std::endl;
}

void TuringMachine::print(std::ostream& ost) const {
    if (currentState_ != 0) {
        ost << "TIME LIMIT EXCEEDED";
        return;
    }
    
    int startTapeCellIdx = tapeSize_;
    
    for (int tapeCellIdx = 0; tapeCellIdx < tapeSize_; ++tapeCellIdx)
        if (tape_[tapeCellIdx] != BLANK_SYMBOL) {
            startTapeCellIdx = tapeCellIdx;
            break;
        }
    
    if (startTapeCellIdx == tapeSize_) {
        ost << "0\n(0)\n";
        ost << "Iterations: " << numIterations_ << std::endl;
        return;
    }
    
    int endTapeCellIdx = findEndTapeCellIdx();
    
    for (int tapeCellIdx = startTapeCellIdx; tapeCellIdx < endTapeCellIdx + 1; ++tapeCellIdx)
        ost << tape_[tapeCellIdx];
        
    ost << std::endl;
    ost << '(' << (endTapeCellIdx - startTapeCellIdx + 1) << ')' << std::endl;
    ost << "Iterations: " << numIterations_ << std::endl;
}

std::string firstMatch(std::string& str, const std::string& reg) {
    std::smatch match;
    std::regex_search (str, match, std::regex(reg));
    std::string result = match[0].str();
    str = match.suffix().str();
    return result;
}

Rule TuringMachine::parseInputLine(const std::string& inputLine) {
    if ( !std::regex_match(inputLine, std::regex("q\\d+ .->q(\\d+|\\*) .(R|L)?")) )
        throw std::logic_error("Incorrect format");
    
    std::string croppedInputLine = inputLine;

    std::string startState = firstMatch(croppedInputLine, "\\d+");
    char startSymbol = firstMatch(croppedInputLine, " .")[1];
    std::string endState = firstMatch(croppedInputLine, "(\\d+|\\*)");
    char endSymbol = firstMatch(croppedInputLine, " .")[1];
    
    if (endState == "*")
        endState = "-1";
    
    Direction direction = NONE;
    
    if (croppedInputLine == "R")
        direction = RIGHT;
    else if (croppedInputLine == "L")
        direction = LEFT;
    
    return Rule { std::stoi(startState) + 1, { true, startSymbol, endSymbol, std::stoi(endState) + 1, direction } };
}
