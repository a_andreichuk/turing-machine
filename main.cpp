//
//  main.cpp
//  TuringMachine
//
//  Created by Anastasiia Andreichuk on 6/14/17.
//  Copyright © 2017 Anastasiia Andreichuk. All rights reserved.
//

#include <iostream>
#include "TuringMachine.hpp"
#include <string>

int main(int argc, const char * argv[]) {
    try {
        std::string inputFile = "input.txt";
        TuringMachine turingMachine(inputFile);
        turingMachine.setShowTape(true);
        turingMachine.run();
        turingMachine.print();
    } catch (std::exception ex) {
        std::cerr << ex.what();
        return 1;
    }
    return 0;
}
