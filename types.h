//
//  types.h
//  TuringMachine
//
//  Created by Anastasiia Andreichuk on 6/14/17.
//  Copyright © 2017 Anastasiia Andreichuk. All rights reserved.
//

#ifndef types_h
#define types_h

enum Direction {
    NONE,
    RIGHT,
    LEFT
};

struct CellRule {
    bool isActive;
    char startSymbol;
    char endSymbol;
    int endState;
    Direction direction;
};

struct Rule {
    int startState;
    CellRule cellRule;
};

#endif /* types_h */
